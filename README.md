**ScopeFun hardware**

This repository contains Scopefun hardware schematics and pcb layout files.

Licensed under CERN OHL v.1.2

You will need Kicad to open hardware design files